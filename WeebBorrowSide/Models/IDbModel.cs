﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace WeebBorrowSide.Models
{
	public interface IDbModel
	{
		int Id { get; set; }
		string UpdateSql { get; }
		string DeleteSql { get; }
		string InsertSql { get; }
		string ReloadSql { get; }

		object this[string fieldName] { get; set; }

		int Update(object replaceParameter);
		void Delete(object replaceParameter);
		void Reload();
		int Insert(object replaceParameter);
		int Update(IDbConnection dbConnection, object replaceParameter);
		void Delete(IDbConnection dbConnection, object replaceParameter);
		void Reload(IDbConnection dbConnection);
		int Insert(IDbConnection dbConnection, object replaceParameter);
	}
}
