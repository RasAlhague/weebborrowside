﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeebBorrowSide.Models
{
	public enum WeebStuffType
	{
		Anime,
		Manga,
		LightNovel
	}
}
