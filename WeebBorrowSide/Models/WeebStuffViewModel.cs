﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace WeebBorrowSide.Models
{
	public abstract class WeebStuffViewModel : DbModel
	{
		public static string UpdateSqlCondition => $@"Update {FullTableSchemaName}
													  SET name = @name, description = @desc, image = @img, borrowing_id = @bId, lending_id = @lId, weeb_stuff_type_id = @type
													  WHERE id_user = @id";
		public static string DeleteSqlCondition => $@"DELETE FROM {FullTableSchemaName}
													  WHERE {PrimaryKeyFieldName} = @id";
		public static string InsertSqlCondition => $@"INSERT INTO {FullTableSchemaName} (id_user, name, password, image)
													  VALUES (@id, @name, @pw, @img);
													  SELECT LAST_INSERT_ID();";
		public static string ReloadSqlCondition => $@"SELECT * 
													  FROM {FullTableSchemaName}
													  WHERE {PrimaryKeyFieldName} = @id";
		public static string TableSchemaName => "tbl_weeb_stuff";
		public static string FullTableSchemaName => "weeb_db.tbl_weeb_stuff";
		public static string PrimaryKeyFieldName => "id_weeb_stuff";
		public string Name { get; set; }
		public string Description { get; set; }
		public string Image { get; set; }
		public int BorrowingUserId { get; set; }
		public int LendingUserId { get; set; }
		public override string UpdateSql => UpdateSqlCondition;
		public override string DeleteSql => DeleteSqlCondition;
		public override string InsertSql => InsertSqlCondition;
		public override string ReloadSql => ReloadSqlCondition;
		public abstract WeebStuffType WeebStuffType { get; }

		public override void Reload()
		{
			throw new NotImplementedException();
		}

		public override void Reload(IDbConnection dbConnection)
		{
			throw new NotImplementedException();
		}
	}
}
