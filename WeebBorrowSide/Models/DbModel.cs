﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace WeebBorrowSide.Models
{
	public abstract class DbModel : IDbModel
	{
		public int Id { get; set; }

		public abstract string UpdateSql { get; }

		public abstract string DeleteSql { get; }

		public abstract string InsertSql { get; }

		public abstract string ReloadSql { get; }

		public object this[string fieldName] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

		public virtual void Delete(object replaceParameter)
		{
			using (var con = new MySql.Data.MySqlClient.MySqlConnection(Configuration.ConnectionString))
			{
				con.Query<UserViewModel>(DeleteSql, replaceParameter);
			}
		}

		public virtual void Delete(IDbConnection dbConnection, object replaceParameter)
		{
			dbConnection.Query<UserViewModel>(DeleteSql, replaceParameter);
		}

		public virtual int Insert(object replaceParameter)
		{
			using (var con = new MySql.Data.MySqlClient.MySqlConnection(Configuration.ConnectionString))
			{
				int id = con.ExecuteScalar<int>(InsertSql, replaceParameter);

				Id = id;

				return id;
			}
		}

		public virtual int Insert(IDbConnection dbConnection, object replaceParameter)
		{
			int id = dbConnection.ExecuteScalar<int>(InsertSql, replaceParameter);

			Id = id;

			return id;
		}

		public abstract void Reload();

		public abstract void Reload(IDbConnection dbConnection);

		public virtual int Update(object replaceParameter)
		{
			using (var con = new MySql.Data.MySqlClient.MySqlConnection(Configuration.ConnectionString))
			{
				con.Query<int>(UpdateSql, replaceParameter);

				return Id;
			}
		}

		public virtual int Update(IDbConnection dbConnection, object replaceParameter)
		{
			dbConnection.Query<int>(UpdateSql, replaceParameter);

			return Id;
		}
	}
}
