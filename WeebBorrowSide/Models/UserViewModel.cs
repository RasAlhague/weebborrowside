﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace WeebBorrowSide.Models
{
	public class UserViewModel : DbModel
	{
		public static string UpdateSqlCondition => $@"Update {FullTableSchemaName}
													  SET name = @name, password = @pw, image = @img
													  WHERE id_user = @id";
		public static string DeleteSqlCondition => $@"DELETE FROM {FullTableSchemaName}
													  WHERE {PrimaryKeyFieldName} = @id";
		public static string InsertSqlCondition => $@"INSERT INTO {FullTableSchemaName} (id_user, name, password, image)
													  VALUES (@id, @name, @pw, @img);
													  SELECT LAST_INSERT_ID();";
		public static string ReloadSqlCondition => $@"SELECT * 
													  FROM {FullTableSchemaName}
													  WHERE {PrimaryKeyFieldName} = @id";
		public static string TableSchemaName => "tbl_user";
		public static string FullTableSchemaName => "weeb_db.tbl_user";
		public static string PrimaryKeyFieldName => "id_user";
		public string Name { get; set; }
		public string Password { get; set; }
		public string Image { get; set; }
		public override string UpdateSql => UpdateSqlCondition;
		public override string DeleteSql => DeleteSqlCondition;
		public override string InsertSql => InsertSqlCondition;
		public override string ReloadSql => ReloadSqlCondition;

		public override void Reload()
		{
			using (var con = new MySql.Data.MySqlClient.MySqlConnection(Configuration.ConnectionString))
			{
				var model = con.Query<UserViewModel>(ReloadSql, new { id = Id })
							   .FirstOrDefault();

				if (model != null)
				{
					Id = model.Id;
					Name = model.Name;
					Password = model.Password;
					Image = model.Image;
				}
			}
		}

		public override void Reload(IDbConnection dbConnection)
		{
			var model = dbConnection.Query<UserViewModel>(ReloadSql, new { id = Id })
									.FirstOrDefault();

			if (model != null)
			{
				Id = model.Id;
				Name = model.Name;
				Password = model.Password;
				Image = model.Image;
			}
		}
	}
}
