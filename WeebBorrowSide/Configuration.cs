﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeebBorrowSide
{
	public static class Configuration
	{
		public static string ConnectionString { get; private set; }

		public static void GetConfigFromEnvironment()
		{
			ConnectionString = Environment.GetEnvironmentVariable("CON_STRING");
		}
	}
}
